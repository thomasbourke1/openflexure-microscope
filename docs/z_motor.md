## Attach the z motor {pagestep}

![](renders/mount_motors_{{var_optics, default:rms}}2.png)
![](renders/mount_motors_{{var_optics, default:rms}}3.png)

* Attach the motor to the z-actuator in the same way as they x and y actuators
* Feed the motor cable down the rectangular slot to the left of the actuator.