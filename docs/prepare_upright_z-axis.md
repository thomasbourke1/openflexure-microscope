# Prepare the separate z-actuator

{{BOM}}

[utility knife]: parts/tools/utility-knife.md
[precision wire cutters]: parts/tools/precision-wire-cutters.md "{cat:tool}"

## Removing brim and supports {pagestep}

The [separate z-actuator](fromstep){cat: PrintedPart, qty:1} has some custom supports and a custom brim to remove. These are highlighted in red in the following images.

![](images/upright/rectangular_z_axis_with_smart_brim.jpg)
![](images/upright/preparing_rectangular_z_axis.jpg)

* Cut the ties inside actuator column (2 total) with the [precision wire cutters]{qty:1}

[prepared separate z-actuator]{output, qty:1, hidden}