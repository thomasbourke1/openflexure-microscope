---
PartData:
    Specs:
        Parfocal Length: 45 mm
        Type (conjugate): Finite Conjugate
        Tube length: 160 mm (Designed for DIN standard microscope with 160 mm mechanical tube length)
        Correction: Plan
        Magnification: 4x-100x (40x recommended)
        Thread type: RMS
---
 
# Microscope Objective

As standard the OpenFlexure Microscope uses a finite conjugate, plan corrected, Microscope objective, with 45mm parfocal length. The magnification depends on what you want to look at. We recommend 40x, as with lower magnification the microscope stage may be very slow.

See our [information page about how the OpenFlexure optics are designed](../../info_pages/imaging_optics_explanation.md) for further information.
